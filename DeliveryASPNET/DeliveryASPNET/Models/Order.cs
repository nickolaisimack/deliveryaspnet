﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryASPNET.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string SenderAddress { get; set; }
        public string RecipientAddress { get; set; }
        public int PackageWeight { get; set; }
        public string PackageSendDate { get; set; }
    }
}
