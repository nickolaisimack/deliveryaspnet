# Delivery form application

This is web application created for delivery orders. You can create new delivery order, check the list of existing orders.

You can check all information about the order, edit or delete it.